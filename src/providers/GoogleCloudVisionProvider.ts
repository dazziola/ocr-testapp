import { Injectable, OnInit } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';

@Injectable()
export class GoogleCloudVisionProvider {

    public projectId = 957258449447;
    public bucketName = 'my-new-bucket';

    constructor(
        public http: Http
    ) {

    }


    checkImage(image: string, key: string) {
        return new Promise((resolve, reject) => {

            let arr = image.split(',');
            let body = {
                "requests": [
                    {
                        "features": [
                            {
                                "type": "TEXT_DETECTION"
                            }
                        ],
                        "image": {
                            "content": arr[1]
                        }
                    }
                ]
            };

            this.http.post('https://vision.googleapis.com/v1/images:annotate?key=' + key, body).subscribe(res => {
                resolve(res);
            });

        });
    }

}
