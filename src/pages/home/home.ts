import { Component, OnInit } from '@angular/core';

import { NavController, ActionSheetController, LoadingController, ModalController } from 'ionic-angular';
import { Camera } from 'ionic-native';

import { GoogleCloudVisionProvider } from '../../providers/GoogleCloudVisionProvider';
import { ResultsPage } from '../results/results';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage implements OnInit {
  srcImage: string;
  OCRAD: any;

  constructor(
    public navCtrl: NavController,
    public actionSheetCtrl: ActionSheetController,
    public loadingCtrl: LoadingController,
    public gcvProvider: GoogleCloudVisionProvider,
    public modalCtrl: ModalController
  ) { }

  ngOnInit() {
  }

  presentActionSheet() {
    const actionSheet = this.actionSheetCtrl.create({
      buttons: [
        {
          text: 'Choose Photo',
          handler: () => {
            this.getPicture(0); // 0 == Library
          }
        }, {
          text: 'Take Photo',
          handler: () => {
            this.getPicture(1); // 1 == Camera
          }
        }, {
          text: 'Demo Photo',
          handler: () => {
            this.srcImage = 'assets/img/receipt.jpg';
          }
        }, {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  getPicture(sourceType: number) {
    // You can check the values here:
    // https://github.com/driftyco/ionic-native/blob/master/src/plugins/camera.ts
    Camera.getPicture({
      quality: 100,
      destinationType: 0, // DATA_URL
      sourceType,
      allowEdit: true,
      saveToPhotoAlbum: false,
      correctOrientation: true
    }).then((imageData) => {
      this.srcImage = `data:image/jpeg;base64,${imageData}`;
    }, (err) => {
      console.log(`ERROR -> ${JSON.stringify(err)}`);
    });
  }

  getDataUri(url, callback) {
    var image = new Image();

    image.onload = function () {
      var canvas = document.createElement('canvas');
      canvas.width = this.naturalWidth; // or 'width' if you want a special/scaled size
      canvas.height = this.naturalHeight; // or 'height' if you want a special/scaled size

      canvas.getContext('2d').drawImage(this, 0, 0);
      // Get raw image data
      // callback(canvas.toDataURL('image/png').replace(/^data:image\/(png|jpg);base64,/, ''));
      // ... or get as Data URI
      let b64 = canvas.toDataURL('image/jpeg');
      callback(canvas.toDataURL('image/jpeg'));
    };

    image.src = url;
  }

  parseResult(res: Object) {
    // res.responses[0].textAnnotations.forEach(text => {
    //   console.log(text.description);
    // });
    let modal = this.modalCtrl.create(ResultsPage, { data: res.responses[0].textAnnotations[0] });
    // Only select the full blob of text, every index after 0 is individual words that has more meta data
    
    modal.present();
  }

  analyze() {

    let loader = this.loadingCtrl.create({
      content: 'Please wait...'
    });
    loader.present();

    // var reader = new FileReader();
    // reader.onload = function (e) {
    //   var data = this.result;
    // }
    // reader.readAsDataURL(file);

    let uri = document.getElementById('image').src;
    let gResult;

    if (uri.substring(0, 10) === "data:image") {
      this.gcvProvider.checkImage(uri, 'AIzaSyAqmWvrjHDXujlUZpJQPMNi7bijko43k04').then(res => {
        let result = JSON.parse(res._body);
        this.parseResult(result);
        loader.dismissAll();
      }); 
    } else {
      this.getDataUri(uri, (res) => {
        this.gcvProvider.checkImage(res, 'AIzaSyAqmWvrjHDXujlUZpJQPMNi7bijko43k04').then(res => {
            let result = JSON.parse(res._body);
            this.parseResult(result);
            loader.dismissAll();
        });
      });
    }

    // loader.dismissAll();
    // (<any>window).OCRAD(document.getElementById('image'), text => {
    //   
    //   alert(text);
    //   console.log(text);
    // });
  }

  restart() {
    this.srcImage = '';
    this.presentActionSheet();
  }

}
