import { Component, OnInit } from '@angular/core';

import { ViewController, NavParams } from 'ionic-angular';

@Component({
  selector: 'page-results',
  templateUrl: 'results.html'
})
export class ResultsPage implements OnInit {

  constructor(
    public viewCtrl: ViewController,
    public params: NavParams
  ) { }

  public data;

  ngOnInit() {
    this.data = this.params.get('data');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  cutUpSentence(sentence: string) {
    console.log(sentence);
    // let nSplits = sentence.split('\n');
    // nSplits.forEach( val => {
    //   return '<div>'+val+'</div>';
    // });
  }
  
}
