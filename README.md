# Ionic 2 OCR Example

## Getting Started
```
$ git clone https://dazziola@bitbucket.org/dazziola/ocr-testapp.git
$ cd ionic-ocr-example
$ npm i
$ ionic state restore
$ ionic serve
```

### Remember to install cordova camera if not already in Ionic Plugins!

## Plugins (using Ionic Native)
* [cordova-plugin-camera]
